# Makefile for Latex
# Matt Stasch <matt.stasch@gmail.com>
# 2012.09.10

DEFAULT_GOAL=all

ifeq ($(OS),Windows_NT)
	RM = del /Q
	FixPath = $(subst /,\,$1)
	SHELL=C:/Windows/System32/cmd.exe
else
	ifeq ($(shell uname), Linux)
		RM = rm -f
		FixPath = $1
	endif
endif

OPT=-c-style-errors -interaction=nonstopmode -quiet

LATEX=xelatex $(OPT)
BIBTEX=bibtex

FILE=thesis

all: $(FILE).pdf

rebuild: clean all

clean:
	@echo Cleaning...
	$(RM) chapters.tex
	$(RM) *.log *.aux *.bbl *.blg *.toc *.lof *.out
	$(RM) *.pdf *.dvi *.ps
	@echo Cleaning...Done!

$(FILE).pdf:
	@echo Generating bibliography...
	$(LATEX) .\$(FILE).tex
	$(BIBTEX) .\$(FILE)
	@echo Generating bibliography...Done!
	@echo Building PDF...
	$(LATEX) .\$(FILE).tex
	$(LATEX) .\$(FILE).tex
	@echo Building PDF...Done!

pdf: $(FILE).pdf

.PHONY: all clean pdf dvi ps rebuild