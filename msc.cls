\ProvidesClass{msc}

\LoadClass[twoside,12pt,a4paper]{report}
\usepackage{fontspec}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{polski, indentfirst, xltxtra}
\usepackage{titlesec, color, fix-cm}
\usepackage{fancyhdr}
\usepackage{datetime}

\usepackage[font=small,labelfont=bf]{caption}

\setmainfont[Ligatures=TeX]{Linux Libertine O}
\setsansfont{Linux Biolinum O}

\linespread{1.3} %1.3 do interlinii 1.5

 \definecolor{lightgray}{gray}{0.90}

\allsectionsfont{\sffamily}

\titleformat{\chapter}[display]
	{\bfseries\Large}
	{\hfill\fontsize{120}{70}\color{lightgray}\sffamily\textbf{\thechapter}}
	{-2ex}
	{\Huge\sffamily}

\let\tmp\oddsidemargin
\let\oddsidemargin\evensidemargin
\let\evensidemargin\tmp
\reversemarginpar

\pagestyle{fancy}
\fancyhf{}
\fancyhead[LO]{\small\sffamily \nouppercase{\rightmark}}
\fancyhead[R]{\small\sffamily \nouppercase{\leftmark}}
\fancyfoot[C]{\sffamily \thepage}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.0pt}